<!--
 * Project: Milestone 2
 * Author: Ricardo Monreal
 * Date: January 10, 2021
 -->
<!DOCTYPE html>
<html lang="en">
<style>
    h2.welcomeTest{
        text-align: center;
        padding-top: 50px;
    }
    p.welcomeTest {
        text-align: center;

    }
</style>
<?php
 include ('navbar.php'); ?>

<h2 class="welcomeTest"> Welcome!</h2>
<hr>
<p class="welcomeTest">Please <a href="login.php">Login</a> or <a href="register.php">Register</a> </p>

</body>
</html>